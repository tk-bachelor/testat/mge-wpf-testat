﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.UIItems;

namespace ch.hsr.wpf.gadgeothek.test
{

    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// the directory in which the test is running
        /// </summary>
        public string BaseDir => Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        /// <summary>
        /// system under test (wpf app to be tested)
        /// </summary>
        public string SutPath => Path.Combine(BaseDir, $"{nameof(ch.hsr.wpf.gadgeothek.ui)}.exe");

        [TestMethod]
        public void TestMethod1()
        {
            var app = Application.Launch(SutPath);
            var window = app.GetWindow("Gadgeothek", InitializeOption.NoCache);

            var okButton = window.Get<Button>("NewGadget");
            okButton.Click();

            Assert.AreEqual("Hello", "Hello");

            app.Close();
        }
    }
}
