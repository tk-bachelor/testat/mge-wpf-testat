﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ch.hsr.wpf.gadgeothek.domain;
using ch.hsr.wpf.gadgeothek.service;

namespace ch.hsr.wpf.gadgeothek.ui.Views
{
    /// <summary>
    /// Interaction logic for GadgetsTable.xaml
    /// </summary>
    public partial class GadgetsTable : UserControl
    {

        public List<Gadget> Gadgets { get; set; }
        private readonly LibraryAdminService _service;

        public delegate void GadgetsDataChangedHandler();

        public GadgetsTable()
        {
            _service = LibraryServiceManager.GetAdminService();

            InitializeComponent();
            LoadAllGadgets();
            DataContext = this;
        }

        private void LoadAllGadgets()
        {
            Gadgets = _service.GetAllGadgets();
            GadgetsDataGrid.ItemsSource = Gadgets;
        }

        private void NewGadget_Click(object sender, RoutedEventArgs e)
        {
            Gadget g = new Gadget("iPhone XI (Grey)")
            {
                Price = 111.99,
                Manufacturer = "Apple"
            };
            NewEditGadget newGadgetDialog = new NewEditGadget(g, NewEditGadget.ViewMode.New);
            newGadgetDialog.GadgetsDataChanged += LoadAllGadgets;
            newGadgetDialog.ShowDialog();
        }

        private void EditGadget_Click(object sender, RoutedEventArgs e)
        {
            Gadget selectedGadget = ((Gadget)GadgetsDataGrid.SelectedItem).Clone();
            if (selectedGadget == null)
            {
                MessageBox.Show(FindWindow(this), "Please select a gadget to edit", "Error");
            }
            else
            {
                NewEditGadget editGadgetDialog = new NewEditGadget(selectedGadget, NewEditGadget.ViewMode.Edit);
                editGadgetDialog.GadgetsDataChanged += LoadAllGadgets;
                editGadgetDialog.ShowDialog();
            }
        }

        private Window FindWindow(FrameworkElement element)
        {
            if (element == null)
                return null;

            if (element is Window)
            {
                return element as Window;
            }

            return FindWindow((FrameworkElement)element.Parent);
        }

        private void DeleteGadget_Click(object sender, RoutedEventArgs e)
        {
            Gadget selectedGadget = (Gadget)GadgetsDataGrid.SelectedItem;
            if (selectedGadget == null)
            {
                MessageBox.Show(FindWindow(this), "Please select a gadget to delete", "Error");
            }
            else
            {
                MessageBoxResult messageBoxResult = MessageBox.Show($"Do you really want to delete {selectedGadget.Name}",
                    "Delete Confirmation", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    bool result = _service.DeleteGadget(selectedGadget);
                    if (result)
                    {
                        this.LoadAllGadgets();
                    }
                    else
                    {
                        MessageBox.Show(FindWindow(this), "Selected gadget couldn't be deleted");
                    }
                }
            }
        }
    }
}
