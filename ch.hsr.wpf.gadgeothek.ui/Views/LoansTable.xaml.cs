﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ch.hsr.wpf.gadgeothek.domain;
using ch.hsr.wpf.gadgeothek.service;

namespace ch.hsr.wpf.gadgeothek.ui.Views
{
    /// <summary>
    /// Interaction logic for LoansTable.xaml
    /// </summary>
    public partial class LoansTable : UserControl
    {
        public List<Loan> Loans { get; set; }
        private readonly LibraryAdminService _service;

        public LoansTable()
        {
            _service = LibraryServiceManager.GetAdminService();


            InitializeComponent();
            LoadAllLoans();

            var refreshTimer = new Timer(5000);
            refreshTimer.Elapsed += Timer_Elapsed;
            refreshTimer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Dispatcher.Invoke(LoadAllLoans);
        }

        private async void LoadAllLoans()
        {
            Console.WriteLine("Loading loans");

            Loans = await Task.Factory.StartNew(() => _service.GetAllLoans());
            //Loans = _service.GetAllLoans();
            LoansDataGrid.ItemsSource = Loans;
        }
    }
}
