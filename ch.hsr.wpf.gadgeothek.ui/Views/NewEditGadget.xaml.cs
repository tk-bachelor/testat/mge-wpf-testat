﻿using System;
using System.Linq;
using ch.hsr.wpf.gadgeothek.domain;
using System.Windows;
using System.Windows.Controls;
using ch.hsr.wpf.gadgeothek.service;
using Condition = ch.hsr.wpf.gadgeothek.domain.Condition;

namespace ch.hsr.wpf.gadgeothek.ui.Views
{
    /// <summary>
    /// Interaction logic for NewEditGadget.xaml
    /// </summary>
    public partial class NewEditGadget : Window
    {
        public enum ViewMode
        {
            New,
            Edit
        };

        public Gadget Gadget { get; set; }
        public ViewMode GadgetViewMode { get; private set; }

        private LibraryAdminService _service;

        public event GadgetsTable.GadgetsDataChangedHandler GadgetsDataChanged;

        public NewEditGadget(Gadget g, ViewMode m)
        {
            Gadget = g;
            GadgetViewMode = m;
            InitializeComponent();

            //ConditionComboBox.ItemsSource = Enum.GetValues(typeof(Condition)).Cast<Condition>();
            DataContext = this;

            _service = LibraryServiceManager.GetAdminService();
        }

        private void SaveGadget_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Gadget.Validate();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this, $"{ex.Message}");
                return;
            }

            if (GadgetViewMode == ViewMode.New)
            {
                bool result = _service.AddGadget(Gadget);
                if (result)
                {
                    GadgetsDataChanged?.Invoke();
                    this.Close();
                }
                else
                {
                    MessageBox.Show(this, $"Failed to add {Gadget.Name}");
                }
            }
            else
            {
                bool result = _service.UpdateGadget(Gadget);
                if (result)
                {
                    GadgetsDataChanged?.Invoke();
                    this.Close();
                }
                else
                {
                    MessageBox.Show(this, $"Failed to update {Gadget.Name}");
                }
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
