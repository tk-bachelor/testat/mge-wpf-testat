﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ch.hsr.wpf.gadgeothek.service;

namespace ch.hsr.wpf.gadgeothek.ui
{
    public class LibraryServiceManager
    {
        private static readonly LibraryAdminService Instance = new LibraryAdminService(ConfigurationManager.AppSettings["server"].ToString());

        public static LibraryAdminService GetAdminService()
        {
            return Instance;
        }
    }
}
